{ pkgs, pkgs-unstable, ... }:
{
    nixpkgs.config.allowUnfree = true;

    environment.systemPackages = with pkgs; [
        # Default applications
        starship
        firefox
        git
        bat
        fd
        procs
        sd
        du-dust
        ripgrep
        tokei
        tealdeer
        neovim
        eza
        nano
        mpv

        # Gaming
        lutris
        wine
        winetricks

        # Appimage support
        appimage-run

        # Dev
        nixd
        pkgs-unstable.zed-editor

        # Art
        krita
    ];

    # KDE connect
    programs.kdeconnect = {
      enable = true;
    };

    # Direnv-nix for dev envs
    programs.direnv = {
      enable = true;
      package = pkgs.direnv;
      silent = false;
      loadInNixShell = true;
      direnvrcExtra = "";
      nix-direnv = {
        enable = true;
        package = pkgs.nix-direnv;
      };
    };

    # Steam
    programs.steam = {
      enable = true;
      remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
      dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
    };

    # Flatpak support
    services.flatpak.enable = true;

    # Android support
    virtualisation = {
       waydroid.enable = true;
       lxd.enable = true;
    };

    # Fonts
    fonts.packages = with pkgs; [
        nerdfonts
    ];

    # Dynamic linking
    programs.nix-ld.enable = true;

}
