{ pkgs, ... }:

{
    imports =
        [
            ./hardware-configuration.nix
            ../../packages.nix
            ../../user.nix
        ];

    # Bootloader
    boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    tmp.cleanOnBoot = true;

        loader = {
          systemd-boot.enable = true;
          efi.canTouchEfiVariables = true;
        };

    };

    # Experimental features nix
     nix.settings.experimental-features = [ "nix-command" "flakes" ];

    # Networking
    networking = {
        firewall = {
            enable = true;
            allowedTCPPortRanges = [
              #Kde connect
              { from = 1714; to = 1764; }
            ];
            allowedUDPPortRanges = [
              #Kde connect
              { from = 1714; to = 1764; }
            ];
        };
        hostName = "terbius";
        networkmanager.enable = true;
    };

    # Locale
    i18n = {
        defaultLocale = "nl_BE.UTF-8";
    };

    i18n.extraLocaleSettings = {
        LC_ADDRESS = "nl_BE.UTF-8";
        LC_IDENTIFICATION = "nl_BE.UTF-8";
        LC_MEASUREMENT = "nl_BE.UTF-8";
        LC_MONETARY = "nl_BE.UTF-8";
        LC_NAME = "nl_BE.UTF-8";
        LC_NUMERIC = "nl_BE.UTF-8";
        LC_PAPER = "nl_BE.UTF-8";
        LC_TELEPHONE = "nl_BE.UTF-8";
        LC_TIME = "nl_BE.UTF-8";
    };
    services.xserver = {
        enable = true;
        xkb.layout = "us";
        xkb.variant = "";
    };

    time.timeZone = "Europe/Brussels";

    # Hardware services
    hardware.opentabletdriver.enable = true;
    hardware.bluetooth.enable = true;
    hardware.pulseaudio.enable = false;
    services.pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
        jack.enable =true;
    };

    # Printers
    services.printing.enable = true;
    services.printing.extraConf = "sides=two-sided-long-edge";
    services.avahi.enable = true;
    services.avahi.nssmdns4 = true;
    services.avahi.openFirewall = true;

    # Virtualisation
    virtualisation = {
        podman = {
            enable = true;
            dockerCompat = true;
        };
    };

    # Desktop env
    services.desktopManager.cosmic.enable = true;
    services.displayManager.cosmic-greeter.enable = true;
 
    # Nixos automatic garbage removal
    nix.settings.auto-optimise-store = true;
    nix.gc = {
        automatic = true;
        dates = "weekly";
        options = "--delete-older-than 30d";
    };

    # Enable openSSH daemon
    services.openssh.enable = true;

    # Nixos version
    system.stateVersion = "24.05";
}
