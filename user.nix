{ pkgs, ... }:
{

    users.users.lennert = {
        isNormalUser = true;
        description = "Lennert";
        extraGroups = ["networkmanager" "wheel"];
    };

    users.defaultUserShell = pkgs.zsh;
    programs.zsh.enable = true;
    environment.shells = with pkgs; [zsh];
}
